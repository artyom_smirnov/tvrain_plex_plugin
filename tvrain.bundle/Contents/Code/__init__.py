VIDEO_PREFIX = "/video/tvrain"

NAME = L('Title')

ART  = 'art.png'
ICON = 'icon.png'

####################################################################################################

def Start():
    Plugin.AddPrefixHandler(VIDEO_PREFIX, VideoMainMenu, NAME, ICON, ART)

    Plugin.AddViewGroup("InfoList", viewMode="InfoList", mediaType="items")
    Plugin.AddViewGroup("List", viewMode="List", mediaType="items")

    MediaContainer.title1 = NAME
    MediaContainer.viewGroup = "List"
    MediaContainer.art = R(ART)
    DirectoryItem.thumb = R(ICON)
    VideoItem.thumb = R(ICON)
    
    HTTP.CacheTime = CACHE_1HOUR

def VideoMainMenu():
    dir = MediaContainer(mediaType='video')

    channels = (
        '1m',
        '640k',
        '340k')

    for c in channels:
        title = '%s (%s)' % (L('Live'), c)
        clip='TVRain_%s.stream' % c
        dir.Append(Function(WebVideoItem(Play, title, thumb=R(ICON)), clip=clip))
    
    return dir

def Play(sender, clip):
    u="rtmp://tvrain-video.ngenix.net/secure/"
    w=640
    h=360
    return Redirect(RTMPVideoItem(url=u, clip=clip, live=True, width=w, height=h))
