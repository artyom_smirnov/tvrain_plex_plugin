#!/bin/sh

DEST="/Users/$USER/Library/Application Support/Plex Media Server/Plug-ins/"

echo Cleaning old plugin version
rm -rf "$DEST/tvrain.bundle"

echo Installing new plugin version
cp -r tvrain.bundle "$DEST"
